import React, {Component} from 'react';
import {Button, Text, View} from 'react-native';
import {connect} from 'react-redux';

export class Counter extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#fff', alignItems: 'center'}}>
        <Text>Counter App</Text>
        <Text>{this.props.count}</Text>
        <Button
          onPress={() => {
            this.props.decreaseCounter();
          }}
          title="Increment"
        />

        <Button
          onPress={() => {
            this.props.increaseCounter();
          }}
          title="Decrement"
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    count: state.count,
  };
}

function mapDispatch(dispatch) {
  return {
    increaseCounter: () => dispatch({type: 'INCREASE_COUNTER'}),
    decreaseCounter: () => dispatch({type: 'DECREASE_COUNTER'}),
  };
}

export default connect(mapStateToProps)(Counter);
