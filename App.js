import React, {Component} from 'react';
import Counter from './components/Counter';
import {createStore} from 'redux';
import {ActionSheetIOS} from 'react-native';
import {Provider} from 'react-redux';

const initialState = {
  count: 0,
};
const reducer = (state = initialState, action) => {
  return state;
};
const store = createStore(reducer);

export class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Counter />
      </Provider>
    );
  }
}

export default App;
